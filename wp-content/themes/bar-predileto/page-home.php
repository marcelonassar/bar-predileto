<?php
// Template Name: Home 
?>

<?php get_header(); ?>

    <section class="container-fluid">
      <div class="row bg-primary text-white">
          <div class="col-lg-7 p-0">
            <div id="carouselApresentacao" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#carouselApresentacao" data-slide-to="0" class="active"></li>
                <li data-target="#carouselApresentacao" data-slide-to="1"></li>
                <li data-target="#carouselApresentacao" data-slide-to="2"></li>
                <li data-target="#carouselApresentacao" data-slide-to="3"></li>
                <li data-target="#carouselApresentacao" data-slide-to="4"></li>
                <li data-target="#carouselApresentacao" data-slide-to="5"></li>
              </ol>
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <img src="<?php the_field('imagem_apresentacao_1'); ?>" class="d-block w-100" alt="<?php the_field('titulo_apresentacao_1'); ?>">
                  <div class="carousel-caption">
                    <h3 class="display-4"><?php the_field('titulo_apresentacao_1'); ?></h3>
                  </div>
                </div>
                <div class="carousel-item">
                  <img src="<?php the_field('imagem_apresentacao_2'); ?>" class="d-block w-100" alt="<?php the_field('titulo_apresentacao_2'); ?>">
                  <div class="carousel-caption">
                    <h3 class="display-4"><?php the_field('titulo_apresentacao_2'); ?></h3>
                  </div>
                </div>
                <div class="carousel-item">
                  <img src="<?php the_field('imagem_apresentacao_3'); ?>" class="d-block w-100" alt="<?php the_field('titulo_apresentacao_3'); ?>">
                  <div class="carousel-caption">
                    <h3 class="display-4"><?php the_field('titulo_apresentacao_3'); ?></h3>
                  </div>
                </div>
                <div class="carousel-item">
                  <img src="<?php the_field('imagem_apresentacao_4'); ?>" class="d-block w-100" alt="<?php the_field('titulo_apresentacao_4'); ?>">
                  <div class="carousel-caption">
                    <h3 class="display-4"><?php the_field('titulo_apresentacao_4'); ?></h3>
                  </div>
                </div>
                <div class="carousel-item">
                  <img src="<?php the_field('imagem_apresentacao_5'); ?>" class="d-block w-100" alt="<?php the_field('titulo_apresentacao_5'); ?>">
                  <div class="carousel-caption">
                    <h3 class="display-4"><?php the_field('titulo_apresentacao_5'); ?></h3>
                  </div>
                </div>
                <div class="carousel-item">
                  <img src="<?php the_field('imagem_apresentacao_6'); ?>" class="d-block w-100" alt="<?php the_field('titulo_apresentacao_6'); ?>">
                  <div class="carousel-caption">
                    <h3 class="display-4"><?php the_field('titulo_apresentacao_6'); ?></h3>
                  </div>
                </div>
              </div>
              <a class="carousel-control-prev" href="#carouselApresentacao" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Anterior</span>
              </a>
              <a class="carousel-control-next" href="#carouselApresentacao" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Proximo</span>
              </a>
            </div>
          </div>
          
          <div class="col-lg-5 p-4 align-self-center">
            <h1 class="display-4 animated fadeInUp">Bar Predileto</h1>
            <p class="lead animated fadeInUp delay-1s"><?php the_field('mensagem_inicial'); ?></p>
      </div>
    </section>
        
    <section class="container py-5" id="quem-somos">
        <div class="mb-5 text-center">
            <span class="h6 uppercase d-block">Um pouco sobre o bar</span>
            <h2 class="display-4 text-danger">Quem somos</h2>
            <p><?php the_field('texto_quem_somos'); ?></p>
        </div>

        <div class="row">
            <div class="col-md-4" id="horario-funcionamento">
                <h3 class="h4 text-danger uppercase py-3">Horário de funcionamento</h3>
                <ul class="list-unstyled">
                    <li>Terça: <?php the_field('horario_terca-feira'); ?></li>
                    <li>Quarta: <?php the_field('horario_quarta-feira'); ?></li>
                    <li>Quinta: <?php the_field('horario_quinta-feira'); ?></li>
                    <li>Sexta: <?php the_field('horario_sexta-feira'); ?></li>
                    <li>Sábado: <?php the_field('horario_sabado'); ?></li>
                    <li>Domingo: <?php the_field('horario_domingo'); ?></li>
                <ul>
            </div>
            
            <div class="col-md-8 bg-light rounded box-shadow py-3 text-center" id="cardapio">
                <h3 class="h4 uppercase text-center text-primary">Nosso cardápio</h3>
                <div class="row">
                    <?php 
                        $arquivo_bebidas = get_field('arquivo_bebidas');

                        if( $arquivo_bebidas ): ?>
                            <div class="col-sm-6 py-4">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bebidas.png" style="height: 40px; width: 40px;">
                                <a href="<?php echo $arquivo_bebidas['url']; ?>" class="h5">Bebidas</a>
                            </div>
                        <?php endif; 
                    ?>

                    <?php 
                        $arquivo_comidas = get_field('arquivo_comidas');

                        if( $arquivo_comidas ): ?>
                            <div class="col-sm-6 py-4">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/petiscos.png" style="height: 40px; width: 40px;">
                                <a href="<?php echo $arquivo_comidas['url']; ?>" class="h5">Petiscos</a>
                            </div>
                        <?php endif; 
                    ?>

                    <?php 
                        $arquivo_executivo = get_field('arquivo_executivo');

                        if( $arquivo_executivo ): ?>
                            <div class="col-sm-6 py-4">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/executivo.png" style="height: 40px; width: 40px;">
                                <a href="<?php echo $arquivo_executivo['url']; ?>" class="h5">Executivo</a>
                            </div>
                        <?php endif; 
                    ?>

                    <?php 
                        $arquivo_vinhos = get_field('arquivo_vinhos');

                        if( $arquivo_vinhos ): ?>
                            <div class="col-sm-6 py-4">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/vinhos.png" style="height: 40px; width: 40px;">
                                <a href="<?php echo $arquivo_vinhos['url']; ?>" class="h5">Vinhos</a>
                            </div>
                        <?php endif; 
                    ?>

                    <?php 
                        $arquivo_menu_especial = get_field('arquivo_menu_especial');

                        if( $arquivo_menu_especial ): ?>
                            <div class="col-sm-12 py-4">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/especial.png" style="height: 40px; width: 40px;">
                                <a href="<?php echo $arquivo_arquivo_menu_especial['url']; ?>" class="h5">Especial</a>
                            </div>
                        <?php endif; 
                    ?>
                </div>
            </div>
        </div>
    </section>

    <section class="py-5 border-bottom border-danger box-shadow" id="nossos-eventos">
        <div class="container">
            <div class="mb-5 text-center">
                <h2 class="display-4 text-danger">Nossos eventos</h2>
            </div>

            <div class="eventos">

                <?php
                $item_eventos = new WP_Query(
                    array(
                        'posts_per_page'   => 6,
                        'post_type'        => 'eventos',
                        'post_status'      => 'publish',
                        'suppress_filters' => true,
                        'orderby'          => 'post_date',
                        'order'            => 'DESC'
                    )
                );
                ?>
                <?php if($item_eventos -> have_posts()):
                    while($item_eventos -> have_posts()): $item_eventos -> the_post(); ?>

                        <div class="card text-white text-center bg-primary mb-3 ml-1 mr-1" style="width: 18rem;">
                            <img src="<?php the_field('imagem_evento'); ?>" class="d-block w-100 img-evento" alt="<?php the_field('nome_evento'); ?>">
                            <div class="card-header"><?php the_field('data_evento'); ?></div>
                            <div class="card-body">
                                <h5 class="card-title"><?php the_field('nome_evento'); ?></h5>
                                <p class="card-text"><?php the_field('descricao_evento'); ?></p>
                            </div>
                        </div>

                    <?php endwhile; ?>
                <?php else: ?>
                    <p class="h6">Nenhum evento disponível</p>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
    </section>

    <section class="container py-5" id="fale-conosco">
        <div class="mb-5 text-center">
            <span class="h6 uppercase d-block">Deseja nos conhecer melhor?</span>
            <h2 class="display-4 text-danger">Fale conosco</h2>
        </div>

        <div class="row">
            <div class="col-lg mb-3">
                <form action="<?php echo get_template_directory_uri(); ?>/enviar-contato.php" method="post" class="bg-light rounded p-4 box-shadow">
                    <div class="form-group">
                        <label for="nome-cliente">Nome</label>
                        <input type="text" class="form-control" id="nome-cliente" name="nome-cliente">
                    </div>
                    <div class="form-group">
                        <label for="email-cliente">E-mail</label>
                        <input type="email" class="form-control" id="email-cliente" name="email-cliente">
                    </div>
                    <div class="form-group">
                        <label for="mensagem-cliente">Mensagem</label>
                        <textarea class="form-control" id="mensagem-cliente" name="mensagem-cliente" rows="3"></textarea>
                    </div>
                    <button type="submit" class="btn btn-outline-danger btn-lg mb-3">Enviar</button>
                </form>
            </div>

            <div class="col-lg" id="contato">
                <h2 class="h6 uppercase">Contato</h2>
                <?php if( have_rows('conjunto_contato') ):
                        while ( have_rows('conjunto_contato') ) : the_row(); ?>
                            <p><?php the_sub_field('telefone'); ?></p>
                        <?php endwhile;
                endif; ?>

                <h2 class="h6 uppercase">Redes sociais</h2>
                <ul class="list-unstyled">
                    <li><a href="<?php the_field('link_facebook'); ?>" target="_blank" class="btn btn-outline-danger btn-sm mb-2" style="width: 140px;">Facebook</a></li>
                    <li><a href="<?php the_field('link_instagram'); ?>" target="_blank" class="btn btn-outline-danger btn-sm mb-2" style="width: 140px;">Instagram</a></li>
                </ul>

                <h2 class="h6 uppercase">Nosso endereço</h2>
                <!-- https://www.embedgooglemap.net/ -->
                <div class="mapouter"><div class="gmap_canvas"><iframe width="400" height="200" id="gmap_canvas" src="https://maps.google.com/maps?q=bar%20predileto&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.emojilib.com"></a></div><style>.mapouter{position:relative;text-align:right;height:200px;width:400px;}.gmap_canvas {overflow:hidden;background:none!important;height:200px;width:400px;}</style></div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>