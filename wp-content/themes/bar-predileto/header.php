<!DOCTYPE html>
<html lang="en">
<head>
    <title>
        <?php
        if(is_front_page()) { //home
            echo wp_title('');

        } elseif (is_page()) {
            echo wp_title(''); echo ' - ';

        } elseif (is_search()) {
            echo 'Busca - ';

        } elseif (!(is_404()) && (is_single()) || (is_page())) {
            wp_title(''); echo ' - ';

        } elseif (is_404()) {
            echo 'Not Found - ';
            
        } bloginfo('name');
        ?>
    </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.png" type="image/png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

    <?php wp_head(); ?>
</head>

<body>

    <header>
        <nav class="navbar navbar-expand-md navbar-light bg-light fixed-top py-3 box-shadow">
            <div class="container">
                <a class="navbar-brand" href="/bar-predileto/">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-bar.png" alt="Bar Predileto">
                </a>
                
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Abrir navegação">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="#quem-somos">Quem somos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#nossos-eventos">Nossos eventos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#fale-conosco">Fale conosco</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>