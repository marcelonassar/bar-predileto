    <footer class="bg-primary text-white">
        <div class="container py-4">
            <div class="row">
                <div class="col-md-3">
                    <h4 class="h6 uppercase">Sessões</h4>
                    <ul class="list-unstyled">
                        <li><a href="#quem-somos" class="text-white">Quem somos</a></li>
                        <li><a href="#nossos-eventos" class="text-white">Nossos eventos</a></li>
                        <li><a href="#fale-conosco" class="text-white">Fale conosco</a></li>
                    </ul>
                </div>

                <div class="col-md-3 mb-3">
                    <h4 class="h6 uppercase">Redes sociais</h4>
                    <a href="<?php the_field('link_facebook'); ?>" target="_blank">
                        <img style="height: 32px; width: 32px;" class="mr-3" src="<?php echo get_template_directory_uri(); ?>/assets/img/facebook.png" alt="Facebook">
                    </a>
                    <a href="<?php the_field('link_instagram'); ?>" target="_blank">
                        <img style="height: 32px; width: 32px;" src="<?php echo get_template_directory_uri(); ?>/assets/img/instagram.png" alt="Instagram">
                    </a>
                </div>

                <div class="col-md-6 text-right" id="desenvolvido-por">
                    <h4 class="h6 uppercase">Desenvolvido por</h4>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-in.png" alt="IN Junior">
                </div>
            </div>
        </div>

        <div class="bg-danger text-center py-3">
            <p class="mb-0">Bar Predileto &copy; 2019. Todos direitos reservados.</p>
        </div>
    </footer>
    <?php wp_footer(); ?>
</body>
</html>