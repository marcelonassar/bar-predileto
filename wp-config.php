<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'bar-predileto');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         's<>+mO-ELBPxE<E]Ih+u;gx2{7c3_dhk1&ZCh6FrfIiIW>KY;,b>kkDB-Z98u`:~');
define('SECURE_AUTH_KEY',  '~c$NdP4w<}Wb+]jLs|7Zkh_!<}usgeJO][-{.KR-}n#X`HWD[ycYld|:VS+$gUh!');
define('LOGGED_IN_KEY',    'M!LlkMfo{Sf&@,IuC?MrmvT>*KLi>F[+}KwI[{B$O+(4S;f]!ck<)&I)[6GcV</f');
define('NONCE_KEY',        'z,;v`9y6&0mW?L-?e[11_#pYuPs8$h56sei2[t!4_U@P3Pt4J~T1gPX).N/h2FPO');
define('AUTH_SALT',        '1q{ZCf^]<p7 |JL7cCW7uqvKebZ8gC?&]~:5`DD ZE^Sq2j#_6{$3(sl_cbUO_35');
define('SECURE_AUTH_SALT', ',Ws8H~p7 ~2`Bz{Dl`w:*e?bz3=sWN.jT6%hlrZLHRGA)Q^4`ac][oMVTLe/+;aI');
define('LOGGED_IN_SALT',   'vn+iS~-o(kT |mgRb<jf[j(Y}?)>#=kmtMRb&!yN*oQZ1ZO[ehD^m.Ki<rP=hnmZ');
define('NONCE_SALT',       '&wt>Y!{W> :F^z1or6SsJUBsm.`N3C6=bkon`H.yuT-;;p*5&:W9Ri*T{4/!Z`WR');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
